// Imported packages
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:trial_gmaps/location_service.dart';
import 'package:trial_gmaps/search_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // Root of the application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Taravel',
      theme: ThemeData(
        primarySwatch: Colors.yellow,
      ),
      home: const MapSample(),
    );
  }
}

class MapSample extends StatefulWidget {
  const MapSample({Key? key}) : super(key: key);

  @override
  State<MapSample> createState() => MapSampleState();
}

// I think this part is more like declaring the variables
class MapSampleState extends State<MapSample> {
  final Completer<GoogleMapController> _controller =
      Completer<GoogleMapController>();
  final TextEditingController _originController = TextEditingController();
  final TextEditingController _destinationController = TextEditingController();

  final Set<Marker> _markers = <Marker>{};
  final Set<Polyline> _polylines = <Polyline>{};
  final Set<Polygon> _polygons = <Polygon>{};
  List<LatLng> polygonLatLngs = <LatLng>[];

  final int _polygonIdCounter = 1;
  int _polylineIdCounter = 1;

  static const CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  @override
  void initState() {
    super.initState();
    _setMarker(const LatLng(37.42796133580664, -122.085749655962));
    _getCurrentLocation();
  }

// Red Marker
  void _setMarker(LatLng point) {
    setState(() {
      _markers.add(
        Marker(
          markerId: const MarkerId('marker'),
          position: point,
        ),
      );
    });
  }

// Line for the route
  void _setPolyline(List<PointLatLng> points) {
    final String polylineIdVal = 'polyline_$_polylineIdCounter';
    _polylineIdCounter++;

    _polylines.add(
      Polyline(
        polylineId: PolylineId(polylineIdVal),
        width: 3,
        color: Colors.black,
        points: points
            .map(
              (point) => LatLng(point.latitude, point.longitude),
            )
            .toList(),
      ),
    );
  }

// Copied from some documentation
// For getting the current location of the user
  Future<void> _getCurrentLocation() async {
    final location = Location();
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;

    // Check if location services are enabled
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    // Check if the app has location permission
    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    // Get the current location
    _locationData = await location.getLocation();

    // Set the current location as a marker on the map
    _setMarker(LatLng(_locationData.latitude!, _locationData.longitude!));

    // Move the camera to the current location
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(_locationData.latitude!, _locationData.longitude!),
          zoom: 14.0,
        ),
      ),
    );
  }

// The design of the homepage
// Includes the map and the drawer
// Includes starting point and destination text form field
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Taravel'),
      ),
      body: Stack(
        children: [
          // Map in the background
          GoogleMap(
            mapType: MapType.normal,
            initialCameraPosition: _kGooglePlex,
            markers: _markers,
            polylines: _polylines,
            polygons: _polygons,
            onMapCreated: (GoogleMapController controller) {
              _controller.complete(controller);
            },
          ),
          // Container for the starting point and destination text form field
          DraggableScrollableSheet(
            initialChildSize: 0.3,
            minChildSize: 0.3,
            maxChildSize: 0.3,
            builder: (BuildContext context, myScrollController) {
              return ClipRRect(
                borderRadius:
                    const BorderRadius.vertical(top: Radius.circular(30.0)),
                child: Container(
                  color: Colors.white,
                  padding: const EdgeInsets.all(15.0),
                  child: ListView(
                    controller: myScrollController,
                    children: [
                      // This is for the SEARCH text at the top of the container
                      const Padding(
                        padding: EdgeInsets.only(
                            left: 10.0, top: 10.0, bottom: 10.0),
                        child: Text(
                          'SEARCH',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(
                            left: 10.0, right: 10.0, top: 5.0),
                        padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10.0),
                          border: Border.all(
                            color: Colors.yellow,
                          ),
                        ),
                        child: Row(
                          children: [
                            // Text Form Field for the Starting Point
                            Expanded(
                              child: Column(
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      // Navigate to the new page
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => SearchPage(),
                                        ),
                                      );
                                    },
                                    child: TextFormField(
                                      controller: _originController,
                                      style: const TextStyle(
                                        color: Colors.black,
                                      ),
                                      decoration: const InputDecoration(
                                        hintText: 'Starting Point',
                                        hintStyle: TextStyle(
                                          color: Colors.black,
                                        ),
                                      ),
                                      enabled:
                                          false, // Set enabled to false to disable editing
                                      onChanged: (value) {
                                        print(value);
                                      },
                                    ),
                                  ),
                                  const SizedBox(height: 1.0),
                                  // Text Form Field for the Destination
                                  GestureDetector(
                                    onTap: () {
                                      // Navigate to the new page
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => SearchPage(),
                                        ),
                                      );
                                    },
                                    child: Container(
                                      margin: const EdgeInsets.only(top: 2.0),
                                      child: TextFormField(
                                        controller: _destinationController,
                                        style: const TextStyle(
                                          color: Colors.black,
                                        ),
                                        decoration: const InputDecoration(
                                          hintText: 'Destination',
                                          hintStyle: TextStyle(
                                            color: Colors.black,
                                          ),
                                          border: InputBorder.none,
                                        ),
                                        enabled:
                                            false, // Set enabled to false to disable editing
                                        onChanged: (value) {
                                          print(value);
                                        },
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            // Dito ilalagay search icon just in case ibalik
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }

// I think this is for showing the place after clicking the search icon button
  Future<void> _goToPlace(
    double lat,
    double lng,
    boundsNe,
    boundsSw,
  ) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: LatLng(lat, lng), zoom: 12),
      ),
    );

    controller.animateCamera(
      CameraUpdate.newLatLngBounds(
          LatLngBounds(
            northeast: LatLng(boundsNe['lat'], boundsNe['lng']),
            southwest: LatLng(boundsSw['lat'], boundsSw['lng']),
          ),
          25),
    );

    _setMarker(LatLng(lat, lng));
  }

  List<LatLng> _createPoints() {
    // Replace with your logic to generate the points for the polyline
    // For example, you can use a directions API to get the route between the origin and destination
    // and convert the steps of the route to a list of LatLng points.
    return [
      const LatLng(37.42796133580664, -122.085749655962),
      const LatLng(37.4219999, -122.0840575),
      const LatLng(37.424541, -122.080651),
      const LatLng(37.4219999, -122.0840575),
      const LatLng(37.417527, -122.08142),
    ];
  }
}
