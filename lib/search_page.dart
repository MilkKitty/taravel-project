// Imported packages
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:google_place/google_place.dart';
import 'package:trial_gmaps/main.dart';
import 'package:location/location.dart' as loc;

// This is for the search page after clicking starting point/destination text form field
class SearchPage extends StatefulWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  final _startSearchFieldController = TextEditingController();
  final _endSearchFieldController = TextEditingController();

  DetailsResult? startPosition;
  DetailsResult? endPosition;

  late FocusNode startFocusNode;
  late FocusNode endFocusNode;

  late GooglePlace googlePlace;
  List<AutocompletePrediction> predictions = [];
  Timer? _debounce;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    String apiKey = 'AIzaSyBP_Nzd0XNF1TJxoSTiiRy9dkZAoKwrFNU';
    googlePlace = GooglePlace(apiKey);

    startFocusNode = FocusNode();
    endFocusNode = FocusNode();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    startFocusNode.dispose();
    endFocusNode.dispose();
  }

  void autoCompleteSearch(String value) async {
    var result = await googlePlace.autocomplete.get(value);
    if (result != null && result.predictions != null && mounted) {
      print(result.predictions!.first.description);
      setState(() {
        predictions = result.predictions!;
      });
    }
  }

// For button 'getting current location'
  Future<void> getCurrentLocation() async {
    loc.Location location = loc.Location();

    try {
      loc.LocationData currentLocation = await location.getLocation();
      if (currentLocation != null) {
        List<Placemark> placemarks = await placemarkFromCoordinates(
            currentLocation.latitude!, currentLocation.longitude!);
        if (placemarks.isNotEmpty) {
          setState(() {
            _startSearchFieldController.text = 'Current Location';
          });
        }
      }
    } catch (e) {
      print('Error: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.yellow,
        leading: const BackButton(color: Colors.black),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      const Padding(
                        padding: EdgeInsets.only(
                            top: 10.0, bottom: 10.0, right: 250.0),
                        child: Text(
                          'SEARCH',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          TextFormField(
                            controller: _startSearchFieldController,
                            autofocus: false,
                            focusNode: startFocusNode,
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.grey[200],
                              border: InputBorder.none,
                              suffixIcon: _startSearchFieldController
                                      .text.isNotEmpty
                                  ? IconButton(
                                      onPressed: () {
                                        setState(() {
                                          predictions = [];
                                          _startSearchFieldController.clear();
                                        });
                                      },
                                      icon: const Icon(Icons.clear_outlined),
                                    )
                                  : null,
                              hintText: 'Starting Point',
                              hintStyle: TextStyle(
                                fontStyle:
                                    _startSearchFieldController.text.isEmpty
                                        ? FontStyle.normal
                                        : FontStyle.italic,
                                color: Colors.grey,
                              ),
                            ),
                            style: const TextStyle(
                              fontSize: 15,
                              fontStyle: FontStyle.italic,
                            ),
                            onChanged: (value) {
                              if (_debounce?.isActive ?? false) {
                                _debounce!.cancel();
                              }
                              _debounce =
                                  Timer(const Duration(milliseconds: 1000), () {
                                if (value.isNotEmpty) {
                                  //places api
                                  autoCompleteSearch(value);
                                } else {
                                  //clear out the results
                                  setState(() {
                                    predictions = [];
                                    startPosition = null;
                                  });
                                }
                              });
                            },
                          ),
                        ],
                      ),
                      const SizedBox(height: 10),
                      TextFormField(
                        controller: _endSearchFieldController,
                        autofocus: false,
                        focusNode: endFocusNode,
                        decoration: InputDecoration(
                          hintText: 'Destination',
                          hintStyle: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            fontStyle: _endSearchFieldController.text.isEmpty
                                ? FontStyle.normal
                                : FontStyle.italic,
                            color: Colors.grey,
                          ),
                          filled: true,
                          fillColor: Colors.grey[200],
                          border: InputBorder.none,
                          suffixIcon: _endSearchFieldController.text.isNotEmpty
                              ? IconButton(
                                  onPressed: () {
                                    setState(() {
                                      predictions = [];
                                      _endSearchFieldController.clear();
                                    });
                                  },
                                  icon: const Icon(Icons.clear_outlined),
                                )
                              : null,
                        ),
                        style: const TextStyle(
                          fontSize: 15,
                          fontStyle: FontStyle.italic,
                        ),
                        onChanged: (value) {
                          if (_debounce?.isActive ?? false) _debounce!.cancel();
                          _debounce =
                              Timer(const Duration(milliseconds: 1000), () {
                            if (value.isNotEmpty) {
                              //places api
                              autoCompleteSearch(value);
                            } else {
                              //clear out the results
                              setState(() {
                                predictions = [];
                                startPosition = null;
                              });
                            }
                          });
                        },
                      )
                    ],
                  ),
                ),
                // Search Icon Button
                IconButton(
                  padding: const EdgeInsets.only(top: 30.0),
                  onPressed: () async {
                    // When search icon is clicked, it will move to the home page
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const MapSample(),
                      ),
                    );
                  },
                  icon: const Icon(
                    Icons.search,
                    color: Color(0xFF464644),
                  ),
                ),
              ],
            ),
            Container(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: predictions.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    leading: const CircleAvatar(
                      child: Icon(
                        Icons.pin_drop,
                        color: Color.fromARGB(255, 244, 222, 22),
                      ),
                    ),
                    title: Text(
                      predictions[index].description.toString(),
                    ),
                    onTap: () async {
                      final placeId = predictions[index].placeId!;
                      final details = await googlePlace.details.get(placeId);
                      if (details != null &&
                          details.result != null &&
                          mounted) {
                        if (startFocusNode.hasFocus) {
                          setState(() {
                            startPosition = details.result;
                            _startSearchFieldController.text =
                                details.result!.name!;
                            predictions = [];
                          });
                        } else {
                          setState(() {
                            endPosition = details.result;
                            _endSearchFieldController.text =
                                details.result!.name!;
                            predictions = [];
                          });
                        }
                        // If condition when the textformfields for origin and destination is not null (not used for now)
                        if (startPosition != null && endPosition != null) {
                          print('navigate');
                        }
                      }
                    },
                  );
                },
              ),
            ),
            // Button Get Current Location
            Container(
              margin: const EdgeInsets.only(top: 20),
              child: Center(
                child: TextButton(
                  onPressed: getCurrentLocation,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Icon(
                        Icons.my_location,
                        color: Colors.black,
                      ),
                      Text(
                        'Get Current Location',
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
